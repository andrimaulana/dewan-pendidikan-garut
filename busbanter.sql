-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2020 at 12:22 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dewanpendidikan`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `agenda_id` int(11) NOT NULL,
  `agenda_nama` varchar(200) DEFAULT NULL,
  `agenda_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `agenda_deskripsi` text,
  `agenda_mulai` date DEFAULT NULL,
  `agenda_selesai` date DEFAULT NULL,
  `agenda_tempat` varchar(90) DEFAULT NULL,
  `agenda_waktu` varchar(30) DEFAULT NULL,
  `agenda_keterangan` varchar(200) DEFAULT NULL,
  `agenda_author` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`agenda_id`, `agenda_nama`, `agenda_tanggal`, `agenda_deskripsi`, `agenda_mulai`, `agenda_selesai`, `agenda_tempat`, `agenda_waktu`, `agenda_keterangan`, `agenda_author`) VALUES
(2, 'Peluncuran Website Resmi Dewan Pendidikan Garut', '2017-01-21 23:26:33', 'Peluncuran Website Resmi Dewan Pendidikan Garut', '2017-01-04', '2017-01-04', 'Kantor Dewan Pendidikan ', '07.30 - 12.00 WIB', 'Peluncuran Website Resmi Dewan Pendidikan Garut', 'Andriansyah Maulana'),
(3, 'Kegiatan Dewan Pendidikan', '2017-01-21 23:29:49', 'Kegiatan Dewan Pendidikan', '2017-02-17', '2017-02-17', 'Dewan Pendidikan', '07.30 - 12.00 WIB', 'Kegiatan Dewan Pendidikan', 'Andriansyah Maulana'),
(4, 'Rapat Dewan Pendidikan', '2018-11-02 13:56:36', 'Rapat Dewan Pendidikan', '2018-11-03', '2018-11-07', 'Kantor Dewan Pendidikan', '10.30 - 11.30', 'UPPPP OK', 'Andriansyah Maulana'),
(5, 'Sosialisasi Pendidikan', '2020-07-24 02:12:28', 'Sosialisasi Pendidikan', '2020-07-04', '2020-07-09', 'Kantor Dewan Pendidikan', '07.30 - 12.00 WIB', 'Sosialisasi Pendidikan', 'Andriansyah Maulana'),
(6, 'Undangan Silaturahmi', '2020-07-24 02:12:59', 'Sosialisasi Pendidikan', '2020-07-28', '2020-07-30', 'Kantor Dewan Pendidikan', '07.30 - 12.00 WIB', 'adadsd', 'Andriansyah Maulana');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kategori_id` int(11) NOT NULL,
  `kategori_nama` varchar(30) DEFAULT NULL,
  `kategori_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `tatatertib` (
  `tatatertib_id` Text NOT NULL,
  `nama_tatatertib` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kategori_id`, `kategori_nama`, `kategori_tanggal`) VALUES
(1, 'Pendidikan', '2016-09-05 22:49:04'),
(2, 'Politik', '2016-09-05 22:50:01'),
(3, 'Sains', '2016-09-05 22:59:39'),
(5, 'Penelitian', '2016-09-05 23:19:26'),
(6, 'Prestasi', '2016-09-06 19:51:09'),
(13, 'Olah Raga', '2017-01-13 06:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `komentar_id` int(11) NOT NULL,
  `komentar_nama` varchar(30) DEFAULT NULL,
  `komentar_email` varchar(50) DEFAULT NULL,
  `komentar_isi` varchar(120) DEFAULT NULL,
  `komentar_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `komentar_status` varchar(2) DEFAULT NULL,
  `komentar_tulisan_id` int(11) DEFAULT NULL,
  `komentar_parent` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`komentar_id`, `komentar_nama`, `komentar_email`, `komentar_isi`, `komentar_tanggal`, `komentar_status`, `komentar_tulisan_id`, `komentar_parent`) VALUES
(3, 'siswa', 'siswa@gmail.com', 'halo bagus tulisannya', '2018-10-08 01:14:00', '1', 22, 0),
(6, 'Andriansyah Maulana', '', 'terima kasih', '2018-10-08 01:16:17', '1', 22, 5),
(9, 'Budi', 'budi@gmail.com', 'halo budi', '2018-11-02 13:24:23', '1', 22, 0),
(10, 'Andriansyah Maulana', '', 'ok mantap', '2018-11-02 13:25:06', '1', 22, 9);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `pegawai_id` int(11) NOT NULL,
  `pegawai_nip` varchar(20) DEFAULT NULL,
  `pegawai_nama` varchar(70) DEFAULT NULL,
  `pegawai_jenkel` varchar(2) DEFAULT NULL,
  `pegawai_jabatan` varchar(40) DEFAULT NULL,
  `pegawai_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`pegawai_id`, `pegawai_nip`, `pegawai_nama`, `pegawai_jenkel`, `pegawai_jabatan`, `pegawai_photo`) VALUES
(25, '1', 'Dr. Maman Rusmana, M.Pd,', 'L', 'Ketua Dewan Pendidikan Garut', '0519f8315b9c2a1939e4fee320e35891.jpg'),
(30, '2', 'Dr. Agus Rahmat Nugraha', 'L', 'Sekretaris Dewan Pendidikan Garut', '2990193bd3117299b722000694b577c9.jpg'),
(37, '3', 'Drs. H. Mahyar Suara, SH, MH', 'L', 'Anggota Dewan Pendidikan Garut', '3a248c4a0c4cd3028aae960050cc71fa.jpg'),
(38, '4', 'Drs. Dede Sutisna, M.Pd', 'L', 'Anggota Dewan Pendidikan Garut', 'f4d7799cf3012788dd18e5fc300160e0.jpg'),
(39, '5', 'Drs. Imam Tamamu T, M.Pd', 'L', 'Anggota Dewan Pendidikan Garut', '374512d992f0dd4458fd7e943dff755d.jpg'),
(40, '6', 'Drs. Achdiat Kusnadi, M.Pd', 'L', 'Anggota Dewan Pendidikan Garut', '8a983b1080489fd94a89d45597d59358.jpg'),
(41, '7', 'Susilawati, S.Pd, M.Pd', 'P', 'Anggota Dewan Pendidikan Garut', '9e15b4415345af1c204e8551dfeb6623.jpeg'),
(42, '8', 'Gunawan, S.Pd, M.MPd', 'L', 'Anggota Dewan Pendidikan Garut', '39f17fbbf902bfdcf4e06fa6defe169f.jpg'),
(43, '9', ' Dian Hasanudin, SE', 'L', 'Anggota Dewan Pendidikan Garut', 'ef50f21fbd212780e481e55c2dfefd46.jpeg'),
(44, '10', 'Ujang Nurjaman, S.Sos, M.Ag', 'L', 'Anggota Dewan Pendidikan Garut', 'ef2f6b2cc7f234fe7551a08dd55ce4f2.jpg'),
(45, '11', ' Dedi Kurniawan, SE.', 'L', 'Anggota Dewan Pendidikan Garut', '4c40c7d6997a7500de1ee38d32b9b435.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `pengguna_id` int(11) NOT NULL,
  `pengguna_nama` varchar(50) DEFAULT NULL,
  `pengguna_jenkel` varchar(2) DEFAULT NULL,
  `pengguna_username` varchar(30) DEFAULT NULL,
  `pengguna_password` varchar(35) DEFAULT NULL,
  `pengguna_email` varchar(50) DEFAULT NULL,
  `pengguna_nohp` varchar(20) DEFAULT NULL,
  `pengguna_status` int(2) DEFAULT '1',
  `pengguna_level` varchar(3) DEFAULT NULL,
  `pengguna_register` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pengguna_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`pengguna_id`, `pengguna_nama`, `pengguna_jenkel`, `pengguna_username`, `pengguna_password`, `pengguna_email`, `pengguna_nohp`, `pengguna_status`, `pengguna_level`, `pengguna_register`, `pengguna_photo`) VALUES
(1, 'Administrator', 'P', 'admin', '827ccb0eea8a706c4c34a16891f84e7b', 'admin@dewanpendidikan.com', '085318298489', 1, '1', '2016-09-02 23:07:55', 'd9be34aa7967b2d1afd4f26e31029628.jpg'),
(2, 'Ibu Susilawati, S.Pd, M.Pd', 'P', 'ibususilawati', '827ccb0eea8a706c4c34a16891f84e7b', 'susilawati@dewanpendidikan.com', '085318298489', 1, '2', '2018-10-04 09:14:32', '5f7ddfd78a87ed803af8ecfc5c52ea8c.jpg'),
(3, 'Andriansyah Maulana', 'L', 'andri', '827ccb0eea8a706c4c34a16891f84e7b', 'andrimaulana105@gmail.com', '085318298489', 1, '1', '2020-07-24 10:21:43', 'ff9ece09801f78a7e2d068d5635f4cc9.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman_id` int(11) NOT NULL,
  `pengumuman_judul` varchar(150) DEFAULT NULL,
  `pengumuman_deskripsi` text,
  `pengumuman_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `pengumuman_author` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengumuman`
--

INSERT INTO `pengumuman` (`pengumuman_id`, `pengumuman_judul`, `pengumuman_deskripsi`, `pengumuman_tanggal`, `pengumuman_author`) VALUES
(2, 'Pengumuman 4', 'Pengumuman 4\r\n', '2017-01-20 18:16:20', 'Andriansyah Maulana'),
(3, 'Pengumuman Launching Web', 'Pengumuman Launching Web Dewan Pendidikan', '2017-01-22 00:16:16', 'Andriansyah Maulana'),
(4, 'Pengumuman 3', 'Pengumuman 3', '2017-01-22 00:15:28', 'Andriansyah Maulana'),
(5, 'Pengumuman 2', 'Pengumuman 3', '2018-09-28 16:41:00', 'Andriansyah Maulana'),
(6, 'Pengumuman', 'Pengumuman 1', '2018-11-02 13:57:19', 'Andriansyah Maulana'),
(7, 'Pengumuman 5', 'Pengumuman Rapat Tanggal 5', '2020-07-24 02:42:10', 'Andriansyah Maulana'),
(8, 'Pengumuman Pelantikan', 'Pelantikan Dewan Pendidikan Kabupaten Garut  pada hari senin ,tempat :Lap.setda Garut ,9 Desembar 2019', '2020-07-27 10:05:42', 'Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `sambutan`
--

CREATE TABLE `sambutan` (
  `sambutan_id` int(11) NOT NULL,
  `sambutan_isi` varchar(5000) DEFAULT NULL,
  `sambutan_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sambutan`
--

INSERT INTO `sambutan` (`sambutan_id`, `sambutan_isi`, `sambutan_photo`) VALUES
(1, '	Dewan Pendidikan merupakan badan yang bersifat mandiri, tidak mempunyai hubungan hierarkis dengan Dinas Pendidikan Kabupaten maupun dengan lembaga-lembaga pemerintah lainnya. Posisi Dewan Pendidikan maupun Dinas Pendidikan Kabupaten maupun lembaga-lembaga pemerintah lainnya mengacu pada kewenangan (otonomi) masing-masing berdasarkan ketentuan yang berlaku. Dewan Pendidikan dibentuk berdasarkan kesepakatan dan tumbuh dari bawah berdasarkan sosiomasyarakat dan budaya serta sosiodemografis dan nilai ­nilai daerah setempat sehingga lembaga tersebut bersifat otonom yang menganut asas kebersamaan menuju ke arah peningkatan kualitas pengelolaan pendidikan di daerah yang diatur oleh Anggaran Dasar dan Anggaran Rumah Tangga. Kondisi ini hendaknya dijadikan dasar pertimbangan oleh masing-masing pihak atau stakeholder pendidikan di daerah agar tidak terjadi adanya pelanggaran hukum administrasi negara yang mengakibatkan adanya konsekuensi hukum baik perdata maupun pidana di kemudian hari.', '2742dd18cba774cbbb1b296062116ae2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `slider_id` int(11) NOT NULL,
  `slider_nama` varchar(30) DEFAULT NULL,
  `slider_tulisan` varchar(120) DEFAULT NULL,
  `slider_photo` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`slider_id`, `slider_nama`, `slider_tulisan`, `slider_photo`) VALUES
(16, 'Dewan Pendidikan', 'Pelantikan', 'e39317feaf26348d499b1a7d446b97ff.JPG'),
(22, 'Pelantikan', 'Pelantikan', '989d58a786ed9a78e512cf50b45e9de5.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tulisan`
--

CREATE TABLE `tulisan` (
  `tulisan_id` int(11) NOT NULL,
  `tulisan_judul` varchar(100) DEFAULT NULL,
  `tulisan_isi` text,
  `tulisan_tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `tulisan_kategori_id` int(11) DEFAULT NULL,
  `tulisan_kategori_nama` varchar(30) DEFAULT NULL,
  `tulisan_views` int(11) DEFAULT '0',
  `tulisan_gambar` varchar(40) DEFAULT NULL,
  `tulisan_pengguna_id` int(11) DEFAULT NULL,
  `tulisan_author` varchar(40) DEFAULT NULL,
  `tulisan_img_slider` int(2) NOT NULL DEFAULT '0',
  `tulisan_slug` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tulisan`
--

INSERT INTO `tulisan` (`tulisan_id`, `tulisan_judul`, `tulisan_isi`, `tulisan_tanggal`, `tulisan_kategori_id`, `tulisan_kategori_nama`, `tulisan_views`, `tulisan_gambar`, `tulisan_pengguna_id`, `tulisan_author`, `tulisan_img_slider`, `tulisan_slug`) VALUES
(22, 'Artikel Berita', '<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n', '2017-05-17 02:38:21', 6, 'Prestasi', 53, '80e28c725f35bed1fc2d76ec18aba973.png', 1, 'Andriansyah Maulana', 0, 'artikel-berita'),
(23, 'Pelaksanaan Ujian Nasional', '<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n', '2017-05-17 02:41:30', 1, 'Pendidikan', 29, '11d054a95e5dde860dc98d21c5b174d5.png', 1, 'Andriansyah Maulana', 0, 'pelaksanaan-ujian-nasional'),
(24, 'Proses belajar mengajar', '<p>Proses belajar mengajar di sekolah berlangsung menyenangkan. Didukung oleh instruktur yang fun dengan metode mengajar yang tidak biasa. Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel a Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel .</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n\r\n<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n', '2017-05-17 02:46:29', 1, 'Pendidikan', 183, 'a5eb8789b4f33a8d5100409409527db4.jpg', 1, 'Andriansyah Maulana', 0, 'proses-belajar-mengajar'),
(26, 'Kegiatan Sosialisasi', '<p>Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel a Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel Ini adalah sampel artikel artikel Ini adalah sampel artikel Ini adalah sampel artikel.</p>\r\n', '2020-07-24 02:45:23', 2, 'Politik', 0, '0322fab7b0b467529b60e16befb10c5a.jpg', 1, 'Andriansyah Maulana', 0, 'kegiatan-sosialisasi');

--
-- Indexes for dumped tables
--
CREATE TABLE `armada` (
  `armada_id` int(11) NOT NULL,
  `armada_nama` varchar(100) DEFAULT NULL,
  `armada_deskripsi` text,
  `armada_jumlah_set` int(11) DEFAULT NULL,
  `armada_kapasitas` int(11) DEFAULT NULL,
  `armada_fasilitas` text,
 `armada_jenis` varchar(40) DEFAULT NULL,
 `armada_gambar_pertama` varchar(40) DEFAULT NULL,
  `armada_gambar_kedua` varchar(40) DEFAULT NULL,
  `armada_gambar_ketiga` varchar(40) DEFAULT NULL,
  `armada_slug` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






--
-- Indexes for table `agenda`
--
ALTER TABLE `armada`
  ADD PRIMARY KEY (`armada_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`komentar_id`),
  ADD KEY `komentar_tulisan_id` (`komentar_tulisan_id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`pegawai_id`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`pengguna_id`);

--
-- Indexes for table `pengumuman`
--
ALTER TABLE `pengumuman`
  ADD PRIMARY KEY (`pengumuman_id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`slider_id`);

--
-- Indexes for table `tulisan`
--
ALTER TABLE `tulisan`
  ADD PRIMARY KEY (`tulisan_id`),
  ADD KEY `tulisan_kategori_id` (`tulisan_kategori_id`),
  ADD KEY `tulisan_pengguna_id` (`tulisan_pengguna_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `agenda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `kategori_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `komentar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `pegawai_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `pengguna_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengumuman`
--
ALTER TABLE `pengumuman`
  MODIFY `pengumuman_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tulisan`
--
ALTER TABLE `tulisan`
  MODIFY `tulisan_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

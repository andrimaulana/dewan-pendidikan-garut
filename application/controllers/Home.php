<?php
class Home extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_tulisan');
		$this->load->model('model_sambutan');
		
		$this->load->model('model_pengumuman');
		
		$this->load->model('model_slider');
		
		
	}
	function index(){
			//untuk ambil data ditampilkan di foreach halaman front end home nantinya jadi $slider misalkan
			$x['berita']=$this->model_tulisan->get_berita_home();
			$x['datax']=$this->model_pengumuman->get_pengumuman_home();
			$x['slider']=$this->model_slider->get_all_slider();		
			
			$x['pengumuman']=$this->model_pengumuman->get_pengumuman_home();
			$x['sambutan']=$this->model_sambutan->get_all_sambutan();

			

			//untuk hitung jumlah data masing - masing entitas misalkan data guru
			
			$x['tot_pegawai']=$this->db->get('pegawai')->num_rows();
			$x['tot_berita']=$this->db->get('tulisan')->num_rows();
			$x['tot_pengumuman']=$this->db->get('pengumuman')->num_rows();
			$x['tot_agenda']=$this->db->get('agenda')->num_rows();
			
			$x['header'] = $this->load->view('tampilan_frontend/header','',TRUE);
			$x['footer'] = $this->load->view('tampilan_frontend/footer','',TRUE);
			$this->load->view('tampilan_frontend/view_home',$x);
	}

}

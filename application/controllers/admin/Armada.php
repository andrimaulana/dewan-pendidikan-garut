<?php
class Armada extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('model_armada');
		
		//$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->model_armada->get_all_armada();
		$this->load->view('admin/v_armada',$x);
	}
	function add_armada(){
	
		$this->load->view('admin/v_add_armada');
	}
	function get_edit(){
		$kode=$this->uri->segment(4);
		$x['data']=$this->model_armada->get_armada_by_kode($kode);
		
		$this->load->view('admin/v_edit_armada',$x);
	}
	function simpan_armada(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

				$this->load->library('upload', $config);
				
				$this->upload->do_upload('filefoto1');
				$gbr1 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr1['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr1['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$gambarpertama=$gbr1['file_name'];


				$this->upload->do_upload('filefoto2');
				$gbr2 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr2['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr2['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$gambarkedua=$gbr2['file_name'];


				$this->upload->do_upload('filefoto3');
				$gbr3 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr3['file_name'];
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr3['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambarketiga=$gbr3['file_name'];


				$this->upload->do_upload('filefoto4');
				$gbr4 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr4['file_name'];
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr4['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
				$gambarkeempat=$gbr4['file_name'];

				
				$judul=$this->input->post('xjudul');
				$isi=$this->input->post('isi');
				$string   = preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $judul);
				$trim     = trim($string);
				$slug     = strtolower(str_replace(" ", "-", $trim));
                $isi=$this->input->post('isi');
                $set=$this->input->post('set');
                $kapasitas=$this->input->post('kapasitas');
                $fasilitas=$this->input->post('fasilitas');
                $jenis=$this->input->post('jenis');
                $this->model_armada->simpan_armada($judul,$isi,$set,$kapasitas,$fasilitas,$jenis,$gambarpertama,$gambarkedua,$gambarketiga,$gambarkeempat,$slug);
                echo $this->session->set_flashdata('msg','success');
                redirect('admin/armada');
                
                
               
	}



	

	function update_armada(){
				
		        $config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

				$this->load->library('upload', $config);


				$this->upload->do_upload('filefoto1');
				$gbr1 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr1['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr1['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$satu=$this->input->post('gambar_satu');
				$path='./assets/images/'.$satu;
				unlink($path);

				$gambarpertama=$gbr1['file_name'];

				
				$this->upload->do_upload('filefoto2');
				$gbr2 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr2['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr2['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$kedua=$this->input->post('gambar_dua');
				$path='./assets/images/'.$kedua;
				unlink($path);

				$gambarkedua=$gbr2['file_name'];


				//
				$this->upload->do_upload('filefoto3');
				$gbr3 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr3['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr3['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$ketiga=$this->input->post('gambar_ketiga');
				$path='./assets/images/'.$ketiga;
				unlink($path);

				$gambarketiga=$gbr3['file_name'];


				//
				$this->upload->do_upload('filefoto4');
				$gbr4 = $this->upload->data();
				//Compress Image
				
				$config['source_image']='./assets/images/'.$gbr4['file_name'];
				
				$config['quality']= '60%';
				$config['width']= 710;
				$config['height']= 460;
				$config['new_image']= './assets/images/'.$gbr4['file_name'];
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();

				$keempat=$this->input->post('gambar_keempat');
				$path='./assets/images/'.$keempat;
				unlink($path);

				$gambarkeempat=$gbr4['file_name'];



				
				$armada_id=$this->input->post('kode');
				$judul=$this->input->post('xjudul');
				$isi=$this->input->post('isi');
				$string   = preg_replace('/[^a-zA-Z0-9 \&%|{.}=,?!*()"-_+$@;<>\']/', '', $judul);
				$trim     = trim($string);
				$slug     = strtolower(str_replace(" ", "-", $trim));
                $isi=$this->input->post('isi');
                $set=$this->input->post('set');
                $kapasitas=$this->input->post('kapasitas');
                $fasilitas=$this->input->post('fasilitas');
                $jenis=$this->input->post('jenis');
                $this->model_armada->update_armada($armada_id,$judul,$isi,$set,$kapasitas,$fasilitas,$jenis,$gambarpertama,$gambarkedua,$gambarketiga,$gambarkeempat,$slug);
                echo $this->session->set_flashdata('msg','success');
                redirect('admin/armada');
                

}


	function hapus_armada(){
		$kode=$this->input->post('kode');

		$dua=$this->input->post('dua');
		$path='./assets/images/'.$dua;
		unlink($path);

		$tiga=$this->input->post('tiga');
		$path='./assets/images/'.$tiga;
		unlink($path);
		

		$empat=$this->input->post('empat');
		$path='./assets/images/'.$empat;
		unlink($path);
		

		$this->model_armada->hapus_armada($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/armada');
	}

}

<?php
class Sambutan extends CI_Controller{
	function __construct(){
		parent::__construct();
		
		$this->load->model('model_sambutan');
		$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->model_sambutan->get_all_sambutan();
		$this->load->view('admin/v_sambutan',$x);
	}
	
	function simpan_sambutan(){
				
	            $config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                       
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        
	                        $config['quality']= '100%';
	                        
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        
	                        $gambar=$this->input->post('gambar');
							$path='./assets/images/'.$gambar;
							unlink($path);

	                        $photo=$gbr['file_name'];
	                        $id=$this->input->post('id');
							$sambutan=$this->input->post('xsambutan');
							$nama=$this->input->post('nama');
							
							

							$this->model_sambutan->simpan_sambutan($id,$sambutan,$nama,$photo);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/sambutan');
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/sambutan');
	                }
	                
	            }else{
							$id=$this->input->post('id');
							$sambutan=$this->input->post('xsambutan');
							$nama=$this->input->post('nama');
							
							$this->model_sambutan->simpan_sambutan_tanpa_img($id,$sambutan,$nama);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/sambutan');
	            } 

	}


				
	



	

}
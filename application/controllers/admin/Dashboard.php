<?php
class Dashboard extends CI_Controller{
	function __construct(){
		parent::__construct();
		
	}
	function index(){
		if($this->session->userdata('akses')=='1'){
		$x['total_berita']=$this->db->get('tulisan')->num_rows();
		$x['total_pegawai']=$this->db->get('pegawai')->num_rows();
		$x['total_armada']=$this->db->get('armada')->num_rows();
		$x['total_pengumuman']=$this->db->get('pengumuman')->num_rows();
			$this->load->view('admin/v_dashboard',$x);
		}else if($this->session->userdata('akses')=='2'){
			$x['total_berita']=$this->db->get('tulisan')->num_rows();
		$x['total_pegawai']=$this->db->get('pegawai')->num_rows();
		$x['total_armada']=$this->db->get('armada')->num_rows();
		$x['total_pengumuman']=$this->db->get('pengumuman')->num_rows();
			$this->load->view('admin/v_dashboard',$x);

		}else  {
			redirect('administrator');
		}
	
	}
	
}
<?php
class Slider extends CI_Controller{
	function __construct(){
		parent::__construct();
		
		$this->load->model('model_slider');
		$this->load->model('model_pengguna');
		$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->model_slider->get_all_slider();
		$this->load->view('admin/v_slider',$x);
	}
	
	function simpan_slider(){
				$config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        
	                        $config['quality']= '100%';
	                        
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        
	                        

	                        $photo=$gbr['file_name'];
							$nama=$this->input->post('xnama');
							$tulisan=$this->input->post('xtulisan');
				
							$this->model_slider->simpan_slider($nama,$tulisan,$photo);
							echo $this->session->set_flashdata('msg','success');
							redirect('admin/slider');
					}else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/slider');
	                }
	                 
	            }else{
	            	$nama=$this->input->post('xnama');
					$tulisan=$this->input->post('xtulisan');

					$this->model_slider->simpan_slider_tanpa_img($nama,$tulisan);
					echo $this->session->set_flashdata('msg','success');
					redirect('admin/slider');


	            }
				
	}

function update_slider(){
				
	            $config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                        
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        
	                        $config['quality']= '100%';
	                        
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        
	                        $gambar=$this->input->post('gambar');
							$path='./assets/images/'.$gambar;
							unlink($path);

	                        $photo=$gbr['file_name'];
	                        $id=$this->input->post('id');
							$nama=$this->input->post('xnama');
							$tulisan=$this->input->post('xtulisan');
							

							$this->model_slider->update_slider($id,$nama,$tulisan,$photo);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/slider');
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/slider');
	                }
	                
	            }else{
							$id=$this->input->post('id');
							$nama=$this->input->post('xnama');
							$tulisan=$this->input->post('xtulisan');
							
							$this->model_slider->update_slider_tanpa_img($id,$nama,$tulisan);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/slider');
	            } 

	}

function hapus_slider(){
		$id=$this->input->post('id');
		$gambar=$this->input->post('gambar');
		$path='./assets/images/'.$gambar;
		unlink($path);
		$this->model_slider->hapus_slider($id);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/slider');
	}




}
<?php
class Tentang extends CI_Controller{
	function __construct(){
		parent::__construct();
		
		$this->load->model('model_tentang');
		$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->model_tentang->get_all_tentang();
		$this->load->view('admin/v_tentang',$x);
	}
	
	function simpan_tentang(){
				
	            $config['upload_path'] = './assets/images/'; //path folder
	            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
	            $config['encrypt_name'] = TRUE; //nama yang terupload nantinya

	            $this->upload->initialize($config);
	            if(!empty($_FILES['filefoto']['name']))
	            {
	                if ($this->upload->do_upload('filefoto'))
	                {
	                        $gbr = $this->upload->data();
	                        //Compress Image
	                       
	                        $config['source_image']='./assets/images/'.$gbr['file_name'];
	                        
	                        $config['quality']= '100%';
	                        
	                        $config['new_image']= './assets/images/'.$gbr['file_name'];
	                        
	                        $gambar=$this->input->post('gambar');
							$path='./assets/images/'.$gambar;
							unlink($path);

	                        $photo=$gbr['file_name'];
	                        $id=$this->input->post('id');
							$sejarah=$this->input->post('sejarah');
                            $visi=$this->input->post('visi');
                            $misi=$this->input->post('misi');
							
							

							$this->model_tentang->simpan_tentang($id,$sejarah,$visi,$misi,$photo);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/tentang');
	                    
	                }else{
	                    echo $this->session->set_flashdata('msg','warning');
	                    redirect('admin/tentang');
	                }
	                
	            }else{
							$id=$this->input->post('id');
							$sejarah=$this->input->post('sejarah');
                            $visi=$this->input->post('visi');
                            $misi=$this->input->post('misi');
							
							
							
							$this->model_tentang->simpan_tentang_tanpa_img($id,$sejarah,$visi,$misi);
							echo $this->session->set_flashdata('msg','info');
							redirect('admin/tentang');
	            } 

	}


				
	



	

}
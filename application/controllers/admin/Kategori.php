<?php
class Kategori extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url('administrator');
            redirect($url);
        };
		$this->load->model('model_kategori');
		$this->load->library('upload');
	}


	function index(){
		$x['data']=$this->model_kategori->get_all_kategori();
		$this->load->view('admin/v_kategori',$x);
	}

	function simpan_kategori(){
		$kategori=$this->input->post('xkategori');
		$this->model_kategori->simpan_kategori($kategori);
		echo $this->session->set_flashdata('msg','success');
		redirect('admin/kategori');
	}

	function update_kategori(){
		$kode=$this->input->post('kode');
		$kategori=$this->input->post('xkategori');
		$this->model_kategori->update_kategori($kode,$kategori);
		echo $this->session->set_flashdata('msg','info');
		redirect('admin/kategori');
	}
	function hapus_kategori(){
		$kode=$this->input->post('kode');
		$this->model_kategori->hapus_kategori($kode);
		echo $this->session->set_flashdata('msg','success-hapus');
		redirect('admin/kategori');
	}

}

<?php
class Armada extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_armada');
		
	}
	function index(){
		$jum=$this->model_armada->armada();
        $page=$this->uri->segment(3);
        if(!$page):
            $offset = 0;
        else:
            $offset = $page;
        endif;
        $limit=20;
        $config['base_url'] = base_url() . 'armada/index/';
            $config['total_rows'] = $jum->num_rows();
            $config['per_page'] = $limit;
            $config['uri_segment'] = 3;
						//Tambahan untuk styling
	          $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
	          $config['full_tag_close']   = '</ul></nav></div>';
	          $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
	          $config['num_tag_close']    = '</span></li>';
	          $config['cur_tag_open']     = '<li class="page-item"><span class="page-link">';
	          $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
	          $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
	          $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
	          $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
	          $config['prev_tagl_close']  = '</span>Next</li>';
	          $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
	          $config['first_tagl_close'] = '</span></li>';
	          $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
	          $config['last_tagl_close']  = '</span></li>';
            $config['first_link'] = 'Awal';
            $config['last_link'] = 'Akhir';
            $config['next_link'] = 'Next >>';
            $config['prev_link'] = '<< Prev';
            $this->pagination->initialize($config);
            $x['page'] =$this->pagination->create_links();
						$x['data']=$this->model_armada->armada_perpage($offset,$limit);
						
						$x['header'] = $this->load->view('tampilan_frontend/header','',TRUE);
						$x['footer'] = $this->load->view('tampilan_frontend/footer','',TRUE);
						$this->load->view('tampilan_frontend/view_armada',$x);
	}
	function detail($slugs){
		$slug=htmlspecialchars($slugs,ENT_QUOTES);
		$query = $this->db->get_where('armada', array('armada_slug' => $slug));
		if($query->num_rows() > 0){
			$b=$query->row_array();
			$kode=$b['armada_id'];
			
			$data=$this->model_armada->get_bus_by_kode($kode);
			$row=$data->row_array();
			$x['id']=$row['armada_id'];
			$x['title']=$row['armada_nama'];
            $x['gambarketiga']=$row['armada_gambar_ketiga'];
			$x['gambarkedua']=$row['armada_gambar_kedua'];
			$x['gambarkeempat']=$row['armada_gambar_keempat'];
            $x['blog'] =$row['armada_deskripsi'];
            $x['set'] =$row['armada_jumlah_set'];
            $x['kapasitas'] =$row['armada_kapasitas'];
            $x['fasilitas'] =$row['armada_fasilitas'];
            $x['jenis'] =$row['armada_jenis'];
			
			$x['data']=$this->model_armada->armada();
						
			
			$x['slug']=$row['armada_slug'];
			
			$x['header'] = $this->load->view('tampilan_frontend/header','',TRUE);
			$x['footer'] = $this->load->view('tampilan_frontend/footer','',TRUE);
			$x['armada']=$this->model_armada->get_armada_home();
			$this->load->view('tampilan_frontend/view_armada_detail',$x);
		
	}

}

    

		

}

<?php
class Tentang extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('model_tentang');
		
		
	}
	function index(){
		$x['tentang']=$this->model_tentang->get_all_tentang();
		$x['header'] = $this->load->view('tampilan_frontend/header','',TRUE);
		$x['footer'] = $this->load->view('tampilan_frontend/footer','',TRUE);
			
		$this->load->view('tampilan_frontend/view_tentang',$x);
	}
}

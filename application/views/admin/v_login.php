<!DOCTYPE html>
<html lang="en">
    <head>
    <link rel="icon" href="<?php echo base_url() ?>assets/images/Uniga.jpg" type="image/x-icon">
        <meta charset="utf-8" />
        <title>PT. Banter Sarana Prima</title>

        <meta name="description" content="User login page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!--basic styles-->

        <link href="<?php echo base_url();?>assets/login/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo base_url();?>assets/login/css/bootstrap-responsive.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/login/css/font-awesome.min.css" />

        <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
       
        <!--page specific plugin styles-->

        <!--fonts-->

       
        <!--ace styles-->

        <link rel="stylesheet" href="<?php echo base_url();?>assets/login/css/ace.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/login/css/ace-responsive.min.css" />
        <link rel="stylesheet" href="<?php echo base_url();?>assets/login/css/ace-skins.min.css" />

        <script src="<?php echo base_url() ?>assets/login/js/jquery.min.js"></script>

       
        <!--inline styles related to this page-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

    <body class="login-layout" style="background-color:#2283c5;">
        <div class="main-container container-fluid">
            <div class="main-content">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="login-container" style="width:475px;">
                            <div class="row-fluid">
                                <div class="center">
                                    <img style="margin-top: 20px;" width="220" src="<?php echo base_url()?>tampilan/assets/images/logobanter.png">
                                    <h1 class="white">Selamat Datang Di Website</h1>
                                    <h4 class="white">PT. Banter Sarana Indah</h4>
                                </div>
                            </div>

                            <div class="space-6"></div>

                            <div class="row-fluid">
                                <div class="position-relative">
                                    <div id="login-box" class="login-box visible widget-box no-border">
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <h4 class="header blue lighter bigger"  >
                                                   
                                                    Silahkan Masuk
                                                </h4>

                                                <div class="space-6"></div>

                                                
                                                <form method="post" action="<?php echo site_url().'admin/login/auth'?>">
                                                    <fieldset>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="text" style="width:400px;" class="span12" placeholder="Username" name="username" />
                                                                <i class="icon-user"></i>
                                                            </span>
                                                        </label>

                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="password" style="width:400px;" class="span12" placeholder="Password" name="password" />
                                                                <i class="icon-lock"></i>
                                                            </span>
                                                        </label>

                                                        <div class="space"></div>

                                                        <div class="clearfix">
                                                            <button type="submit" class="width-35 pull-right btn btn-small btn-primary">
                                                               
                                                                Masuk
                                                            </button>
                                                        </div>

                                                        <div class="space-4"></div>
                                                    </fieldset>
                                                </form>
                                            </div><!--/widget-main-->

                                            
                                        </div><!--/widget-body-->
                                    </div><!--/login-box-->

                                    <div id="forgot-box" class="forgot-box widget-box no-border">
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <h4 class="header red lighter bigger">
                                                    <i class="icon-key"></i>
                                                    Retrieve Password
                                                </h4>

                                                <div class="space-6"></div>
                                                <p>
                                                    Enter your email and to receive instructions
                                                </p>

                                                <form />
                                                    <fieldset>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="email" class="span12" placeholder="Email" />
                                                                <i class="icon-envelope"></i>
                                                            </span>
                                                        </label>

                                                        <div class="clearfix">
                                                            <button onclick="return false;" class="width-35 pull-right btn btn-small btn-danger">
                                                                <i class="icon-lightbulb"></i>
                                                                Send Me!
                                                            </button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div><!--/widget-main-->

                                            <div class="toolbar center">
                                                <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                                                    Back to login
                                                    <i class="icon-arrow-right"></i>
                                                </a>
                                            </div>
                                        </div><!--/widget-body-->
                                    </div><!--/forgot-box-->

                                    <div id="signup-box" class="signup-box widget-box no-border">
                                        <div class="widget-body">
                                            <div class="widget-main">
                                                <h4 class="header green lighter bigger">
                                                    <i class="icon-group blue"></i>
                                                    Daftar Pengguna Baru
                                                </h4>

                                                <div class="space-6"></div>
                                                <p> Masukan data berikut: </p>

                                                <form method="post" action="<?php echo base_url(); ?>index.php/login/register">
                                                    <fieldset>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="text" name="npm" class="span12" style="width:400px;" placeholder="NPM" required maxlength="10" onkeypress="return isNumberKey(event)" />
                                                                <i class="icon-link"></i>
                                                            </span>
                                                        </label>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="text" name="nama" class="span12" style="width:400px;" placeholder="Nama" required />
                                                                <i class="icon-user"></i>
                                                            </span>
                                                        </label>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <?php 
                                                                    $value = NULL;
                                                                    if($this->input->post('agama') != NULL){
                                                                      $value = $this->input->post('agama');
                                                                    }
                                                                  ?>
                                                                                     
                                                                <select style="width:400px;"  class="form-control" name="agama" required="required">
                                                                <option value="">-- Pilih Agama --</option>
                                                                <option value="Islam"<?php echo ($value == 'Islam') ? "selected" : ""; ?>>Islam</option>
                                                                <option value="Kristen" <?php echo ($value == 'Kristen') ? "selected" : ""; ?>>Kristen</option>
                                                                <option value="Katholik" <?php echo ($value == 'Katholik') ? "selected" : ""; ?>>Katholik</option>
                                                                <option value="Hindu" <?php echo ($value == 'Hindu') ? "selected" : ""; ?>>Hindu</option>
                                                                <option value="Budha" <?php echo ($value == 'Budha') ? "selected" : ""; ?>>Budha</option>
                                                                <option value="Lainnya" <?php echo ($value == 'Lainnya') ? "selected" : ""; ?>>Lainnya</option>
                                                                </select>
                                                            </span>
                                                        </label>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <input type="text" name="No_HP" class="span12" style="width:400px;" placeholder="No HP" required="required" maxlength="12" onkeypress="return isNumberKey(event)" />
                                                                <i class="icon-phone"></i>
                                                            </span>
                                                        </label>
                                                        <label>
                                                            <span class="block input-icon input-icon-right">
                                                                <textarea name="alamat" style="width:400px;" placeholder="Alamat" class="span12"></textarea>
                                                            </span>
                                                        </label>

                                                        <div class="space-24"></div>

                                                        <div class="clearfix">
                                                            <button type="reset" class="width-30 pull-left btn btn-small">
                                                                <i class="icon-refresh"></i>
                                                                Hapus
                                                            </button>

                                                            <button type="submit" class="width-65 pull-right btn btn-small btn-success">
                                                                Daftar
                                                                <i class="icon-arrow-right icon-on-right"></i>
                                                            </button>
                                                        </div>
                                                    </fieldset>
                                                </form>
                                            </div>

                                            <div class="toolbar center">
                                                <a href="#" onclick="show_box('login-box'); return false;" class="back-to-login-link">
                                                    <i class="icon-arrow-left"></i>
                                                    Kembali ke Halaman Login
                                                </a>
                                            </div>
                                        </div><!--/widget-body-->
                                    </div><!--/signup-box-->
                                </div><!--/position-relative-->
                            </div>
                        </div>
                    </div><!--/.span-->
                </div><!--/.row-fluid-->
            </div>
        </div><!--/.main-container-->

        <!--basic scripts-->

        <!--[if !IE]>-->

        <!--inline scripts related to this page-->

        <script type="text/javascript">
            function show_box(id) {
             $('.widget-box.visible').removeClass('visible');
             $('#'+id).addClass('visible');
            }

            function isNumberKey(evt)
            {
              var charCode = (evt.which) ? evt.which : event.keyCode
              if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
              return true;
            }
        </script>
    </body>
</html>

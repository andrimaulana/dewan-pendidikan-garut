<footer class="footer-block bg-black" >
        <div class="container">
            <!-- footer-top-block -->
            <div class="footer-top-block yellow-theme">            
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget_about">    
                            <h3 class="widget-title">
                            PT. Banter Sarana Prima - Bus Banter
                            </h3><!-- /.widget-title -->
                            <div class="widget-about-content">
                                <img src="<?php echo base_url()?>tampilan/assets/images/logobanter.png" alt="logo" />
                                <p>Bus Pariwisata Terbaik Di Kabupaten Garut Yang Siap Menemani Perjalanan Wisata Anda</p>
                                
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget widget_about -->
                    </div><!-- /.col-md-3 -->
                    <div class="col-md-2 col-sm-6">
                        <div class="widget widget_menu">
                            <h3 class="widget-title">
                                
                            </h3><!-- /.widget-title -->
                            
                        </div><!-- /.widget -->
                    </div><!-- /.col-md-3 -->

                    <div class="col-md-3 col-sm-6">
                        <div class="widget widget_hot_contact">
                            <h3 class="widget-title">
                                Kontak Kami
                            </h3><!-- /.widget-title -->
                            <ul>
                                <li><a href="#"><i class="fa fa-envelope"></i>banter@gmail.com</a></li>
                                <li><a href="#"><i class="fa fa-phone"></i>(+880)023985471</a></li>
                                <li><a href="#"><i class="fa fa-map-marker"></i>Jl. Terusan Pembangunan No.56, Jayaraga, Kec. Garut Kota, Kabupaten Garut, Jawa Barat 44151</a></li>
                            </ul> 
                        </div><!-- /.widget -->
                        <!-- /.widget -->
                    </div><!-- /.col-md-3 -->

                    <!-- /.col-md-4 -->
                </div><!-- /.row -->
            </div><!-- /.footer-top-block -->

            <!-- footer-bottom-block -->
            <div class="footer-bottom-block">            
                <div class="row">
                     <div class="col-md-9">
                        <div class="bottom-content-left">
                            <p class="copyright"  style="color:white">Copyright &copy; <?php echo date('Y')?> PT. Banter Sarana Prima - Bus Banter Created by <a href="http://andriansyah.my.id/" target="_blank">Andriansyah Maulana</a></p>
                        </div><!-- /.bottom-top-content -->
                     </div><!-- /.col-md-9 -->
                     <div class="col-md-3">
                        <div class="bottom-content-right">
                            <div class="social-profile">
                                <span class="social-profole-title">Follow Sosial Media Kami:</span>
                                <a href="#"><i class="fa fa-instagram"></i></a>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                
                            </div><!-- /.social-profile -->
                        </div><!-- /.bottom-content-right -->
                     </div><!-- /.col-md-3 -->
                </div><!-- /.row -->
            </div><!-- /.footer-bottom-block -->
        </div><!-- /.container -->
    </footer>
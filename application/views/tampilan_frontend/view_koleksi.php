<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="author" content="SemiColonWeb" />

  <!-- Stylesheets
  ============================================= -->
  <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/dark.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/font-icons.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/magnific-popup.css" type="text/css" />
  <link rel="shorcut icon" type="text/css" href="<?php echo base_url() ?>frontend/images/favicon.png">

  <link rel="stylesheet" href="<?php echo base_url() ?>frontend/css/responsive.css" type="text/css" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- Document Title
  ============================================= -->
  <title>Bus Banter Garut</title>
  
</head>

<body class="stretched">

  <!-- Document Wrapper
  ============================================= -->
  <div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
   <?php echo $header?>
   <!-- #header end -->

    <!-- Page Title
    ============================================= -->
    <section id="page-title">

      <div class="container clearfix">
        <h1>Koleksi Buku </h1>
        <span>Perpustakaan SMAN 11 Garut</span>
        
      </div>

    </section><!-- #page-title end -->

    <!-- Content
    ============================================= -->
    <section id="content">

<div class="content-wrap">

    <div class="container clearfix">

        <div id="side-navigation" class="tabs custom-js">

            

            <div class="col_two_third col_last nobottommargin">
          
                <div id="snav-content1">

                    <h3>Koleksi Buku Perpustakaan SMAN 11 Garut</h3>
                    
                    
                </div>
               
                <?php foreach ($koleksi->result() as $row) :?>
                <div id="snav-content2">
                   
                    <p><?php echo $row->nama_koleksi;?></p>
                </div>
                <?php endforeach;?>
                

                

            </div>

        </div>

    </div>

</div>

</section><!-- #content end -->

    <!-- Footer
    ============================================= -->
   
<?php echo $footer?>
           

            <!-- Copyrights
            ============================================= -->
            <!-- #copyrights end -->

       <!-- #footer end --><!-- #footer end -->

  </div><!-- #wrapper end -->

  <!-- Go To Top
  ============================================= -->
  <div id="gotoTop" class="icon-angle-up"></div>

  <!-- External JavaScripts
  ============================================= -->
  <script type="text/javascript" src="<?php echo base_url() ?>frontend/js/jquery.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>frontend/js/plugins.js"></script>

  <!-- Footer Scripts
  ============================================= -->
  <script type="text/javascript" src="<?php echo base_url() ?>frontend/js/functions.js"></script>

</body>
</html>
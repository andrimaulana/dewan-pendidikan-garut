<!doctype html>
<html lang="en">



<head>
   
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">   
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
    <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <div class="header-modal-area">
        <!-- Modal Search -->
        <div class="overlay overlay-scale">
            <button type="button" class="overlay-close">&#x2716;</button>
            <div class="overlay__content">
                <form id="search-form" class="search-form outer" action="#" method="post">
                    <div class="input-group">
                        <input type="text" class=" input--full" placeholder="search text here ..."> 
                    </div>
                    <button class="btn text-uppercase search-button">Search</button>
                </form>
            </div>
        </div>

        <!-- /.overlay-sidebar -->
    </div><!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
   <?php echo $header?> 
   <!-- /.header-bottom-area -->

    <!-- ====== Page Header ====== -->
    <div class="page-header nevy-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title">Tentang Kami</h2>
                    <p class="page-description yellow-color">PT. Banter Sarana Prima - Bus Banter Garut</p>        
                </div><!-- /.col-md-12 -->
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div>

    <!-- ====== About Main Content ====== --> 
    <div class="about-main-content mr-top-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-top-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="heading-content-three">
                                    <h2 class="title">Sejarah <br />Kami</h2>
                                    
                                </div><!-- /.section-title-area -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                               
                            </div><!-- /.col-md-12 -->
                            <div class="col-md-6">
                            <?php foreach ($tentang->result() as $row) :?>
                                <div class="about-content-left">
                                    <p> <?php echo $row->sejarah;?></p>
                                </div>
                                <?php endforeach;?>
                                <!-- /.about-content-left-->
                            </div><!-- /.col-md-5 -->
                            <?php foreach ($tentang->result() as $row) :?>
                            <div class="col-md-6">
                                <img src="<?php echo base_url().'assets/images/'.$row->tentang_gambar;?>" alt="car-item" />
                            </div>
                            <?php endforeach;?>
                            <!-- /.col-md-7 -->
                        </div><!-- /.row -->
                    </div><!-- /.top-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.about-main-content -->

    <!-- ======driver block======= --> 
    <div class="about-main-content mr-top-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-top-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="heading-content-three">
                                   
                                </div><!-- /.section-title-area -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                            <h4 class="extra-big-title">Visi Dan Misi </h4>
                            </div><!-- /.col-md-12 -->
                            <div class="col-md-6">
                            <?php foreach ($tentang->result() as $row) :?>
                                <div class="about-content-left">
                                    <h3>Visi Kami</h3>
                                    <p> <?php echo $row->visi;?></p>
                                </div>
                                <?php endforeach;?>
                                <!-- /.about-content-left-->
                            </div><!-- /.col-md-5 -->
                            <?php foreach ($tentang->result() as $row) :?>
                            <div class="col-md-6">
                            <div class="about-content-left">
                            <h3>Misi Kami</h3>
                                    <p> <?php echo $row->misi;?></p>
                                </div>
                            </div>
                            <?php endforeach;?>
                            <!-- /.col-md-7 -->
                        </div><!-- /.row -->
                    </div><!-- /.top-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.driver-area -->

    <!-- ======footer area======= -->
    <div class="container footer-top-border">
        <div class="vehicle-multi-border"></div><!-- /.vehicle-multi-border -->
    </div><!-- /.container -->

    <?php echo $footer?>
    <!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->
</body>



</html>
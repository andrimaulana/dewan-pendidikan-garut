<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:48 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    
    <link rel="apple-touch-icon" href="<?php echo base_url()?>tampilan/assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">   
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
   <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <div class="header-modal-area">
        <!-- Modal Search -->
        

        <!-- /.overlay-sidebar -->
    </div><!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
    <?php echo $header ?><!-- /.header-bottom-area -->

    <!-- ====== Page Header ====== -->
    <div class="page-header nevy-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title">Armada</h2>
                    <p class="page-description">Berikut adalah daftar armada kami</p>        
                </div><!-- /.col-md-12 -->
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div>

    <!-- ======blog-area====== --> 
   <div class="blog-block bg-gray-color">
       <div class="container">
           <div class="row">
                <div class="col-md-8">
                    <div class="post-filter-block clearfix">
                        <div class="post-filter-area clearfix">
                           
                        </div> <!-- /.tab-list -->
                    </div><!-- /.post-filter-block -->

                    <div class="ralated-block bg-gray-color">
       <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="ralated-heading text-center">
                        <h2 class="related-title">Armada PT. Banter Sarana Prima</h2>
                    </div><!-- /.blog-heading -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
           <div class="row">
           <?php foreach ($data->result() as $row) : ?>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="vehicle-content theme-yellow">
                        <div class="vehicle-thumbnail">
                            <a href="<?php echo site_url('armada/detail/'.$row->armada_slug);?> ">
                                <img src="<?php echo base_url().'assets/images/'.$row->armada_gambar_kedua;?>" style="height:200px" alt="car-item" />
                            </a>
                        </div><!-- /.vehicle-thumbnail -->
                        <div class="vehicle-bottom-content">
                            <h2 class="vehicle-title"><a href="<?php echo site_url('armada/detail/'.$row->armada_slug);?>"><?php echo $row->armada_nama;?></a></h2>
                            <div class="vehicle-meta">
                                <div class="meta-item">
                                <a href="<?php echo site_url('armada/detail/'.$row->armada_slug);?>"><h4 > Jenis : <?php echo $row->armada_jenis;?> </h4></a>
                                  
                                </div>
                                <div class="meta-item">
                                <a href="<?php echo site_url('armada/detail/'.$row->armada_slug);?>"> <h4>Jumlah Set: <?php echo $row->armada_jumlah_set;?> </h4></a>
                                  
                                </div>
                                <div class="meta-item">
                                <a href="<?php echo site_url('armada/detail/'.$row->armada_slug);?>">  <h4>Kapasitas Armada: <?php echo $row->armada_kapasitas;?> </h4> </a>
                                  
                                </div>
                            </div><!-- /.meta-left -->
                        </div><!-- /.vehicle-bottom-content -->
                    </div><!-- /.car-content -->
                </div><!-- /.col-md-3 -->
                <?php endforeach;?>
                 
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div>
                   
                   
                   
                   <!-- Kode  -->
                   <!-- /.blog-content-left -->
                </div><!-- /.col-md-8 -->

               <!-- /.col-md-4 -->
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div><!-- /.blog-main-content -->
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="pagination-center">
                                <?php echo $page;?>
                                </ul>
                            </div>
    <!-- ======footer area======= -->
    <div class="container footer-top-border">
        <div class="vehicle-multi-border yellow-black"></div><!-- /.vehicle-multi-border -->
    </div><!-- /.container -->

    <?php echo $footer?>
    <!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->
</body>

<!-- Mirrored from htmlguru.net/carrent-html/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:49 GMT -->
</html>
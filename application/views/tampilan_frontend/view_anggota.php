<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:45:44 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>tampilan/images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>tampilan/images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">

    <!-- RS5.4 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/settings.css">
    <!-- RS5.4 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/navigation.css">
    
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
    <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
    <?php echo $header; ?><!-- /.header-bottom-area -->

    <!-- ======= Main Slider Area =======-->
   <!-- /.slider-block -->

    <!-- ====== Section divider ====== --> 
    <!-- /.section-divider -->

    <!-- ====== Check Vehicle Area ====== --> 
   <!-- /.check-vehicle-block-->

         
    <!-- ====== Popular Vehicle Block ====== --> 
    <!-- /.Popular Vehicle Block --> 

    <!-- ======Regular-vehicle-block======= --> 
    <!-- /.Regular-Vehicle Block -->

    <!-- ======fun facts block======= -->
   <!-- /.fun-facts-block -->

    <!-- ======driver block======= --> 
    <div class="container">
            <div class="row tb default-margin-bottom yellow-theme">
                <div class="col-md-9 block-title-area tb-cell">
                    <div class="heading-content style-one border">
                        <h3 class="subtitle">Staf Dan Driver Kami </h3>
                        <h2 class="title">PT. Banter Sarana Prima</h2>
                    </div><!-- /.heading-content-one -->
                </div><!-- /.col-md-10 -->

                <div class="col-md-3 hidden-xs block-navigation-area tb-cell">
                    <div class="pull-right">                    
                        <div class="item-navigation">
                            
                        </div><!-- /.item-navigation -->

                        <div class="view-all-item">
                           
                        </div><!-- /.view-all-item -->
                    </div><!-- /.pull-right -->
                </div><!-- /.col-md-2 -->
            </div>
	<div class="row">
          <?php foreach ($data->result() as $row) : ?>
						<div class="col-md-3 col-sm-6 bottommargin">

							<div class="team">
								<div class="team-image">
                <?php if(empty($row->pegawai_photo)):?>
								 <img src="<?php echo base_url().'assets/images/blank.png';?>" style="width:250px; height: 300px">
                  <?php else:?>
                  <img src="<?php echo base_url().'assets/images/'.$row->pegawai_photo;?>" style="width:250px; height: 300px">
                          <?php endif;?>

								</div>
								<div class="team-desc">
                  <br>
                  <div class="vehicle-bottom-content">
                           <h2 class="driver-name vehicle-title"><a href="#"><?php echo $row->pegawai_nama;?></a></h2>
                            <h4 class="driver-desc"><?php echo $row->pegawai_jabatan;?></h4>
                        </div>			
								</div>
							</div>

						</div>
            <?php endforeach;?>
</div>

    
    <!-- /.driver-carousel -->
           


            
            <!-- block-navigation-area -->
            <div class="block-navigation-area visible-xs-block">
                <!-- /.view-all-item -->
            </div><!-- /.block-navigation-area -->
        </div><!-- /.container  -->
    </div>
    

    <!-- /.driver-area -->

    <!-- ====== App block ====== --> 
    <!-- /.app-block -->

    <!-- ====== Company Brand Block ====== --> 
    <!-- /.company-logo-block -->

    <!-- ======blog area======= --> 
    <!-- /.Blog-area-->

    <!-- ======footer area======= -->
    <!-- /.container -->

<?php echo $footer ?>
    <!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->

    <!-- RS5.4 Core JS Files -->
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
  
    <script>
        jQuery(document).ready(function() {
            var $sliderSelector = jQuery(".carrent-slider");
            $sliderSelector.revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    }
                },
                responsiveLevels:[1400,1368,992,480],
                visibilityLevels:[1400,1368,992,480],
                gridwidth:[1400,1368,992,480],
                gridheight:[600,600,500,380],
                disableProgressBar:"on"
            });
        });
    </script>

    <!-- SLIDER REVOLUTION 5.4 EXTENSIONS  (Load Extensions only on Local File Systems! The following part can be removed on Server for On Demand Loading) -->
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
</body>

<!-- Mirrored from htmlguru.net/carrent-html/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:08 GMT -->
</html>
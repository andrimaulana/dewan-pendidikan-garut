<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:45:44 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo base_url()?>tampilan/assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>tampilan/images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>tampilan/images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">

    <!-- RS5.4 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/settings.css">
    <!-- RS5.4 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>tampilan/assets/revolution/css/navigation.css">
    
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
   <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <div class="header-modal-area">
        <!-- Modal Search -->
        

        <!-- /.overlay-sidebar -->
    </div><!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
    <!-- /.header-bottom-area -->
<?php echo $header?>
    <!-- ======= Main Slider Area =======-->
    <div class="slider-block">    
        <div class="rev_slider_wrapper">
            <div class="rev_slider carrent-slider" id="carrent-slider">
                <ul>
					
					<!-- slide 1 --> 
					<?php foreach ($slider->result() as $row) :?>
                    <li data-transition="fade" data-slotamount="default" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="materialize Material" data-description="">

                        <!-- main image -->
                        <img src="<?php echo base_url().'assets/images/'.$row->slider_photo;?>"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

                        <!-- layer 01 -->
                        

                        <!-- layer 02 -->
                       

                        <!-- LAYER NR. 3 -->
                       
					</li>
					<?php endforeach;?> 
					<!-- /.slide 1 -->
                </ul>             
            </div><!-- /.revolution slider -->
        </div><!-- /.slider wrapper -->
    </div><!-- /.slider-block -->

    <!-- ====== Section divider ====== --> 
    <div class="vehicle-section-divider night-rider">
        <div class="contoiner-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-divider-content">
                        <div class="vehicle-border">
                            <img src="<?php echo base_url()?>tampilan/assets/images/bisbanter.png" alt="car-item" />
                        </div><!-- /.car-border -->
                    </div><!-- /.section-divider-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.section-divider -->

	<!-- ====== Check Vehicle Area ====== --> 
	
	<div class="wellcome-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading-content style-two">
                        <h2 class="title text-uppercase">Selamat Datang Di </h2>
                        <h3 class="subtitle">Bus Banter - Bus Garut Terbaik</h3>
                    </div><!-- /.heading-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-5">
                    <div class="about-service night-rider">
                        <div class="about-title-area">
                            <h2 class="about-service-title">Bus Pariwisata Terbaik Di Kabupaten Garut</h2>
                        </div><!-- /.about-service -->
                        <p class="discription">Kami Memiliki Armada Terbaik Untuk Menunjang Kegiatan Pariwisata Anda</p>
                        <div class="about-image">
                            <img src="<?php echo base_url()?>tampilan/assets/images/banter2.png" alt="car-item" />
                        </div>
                    </div><!-- /.about-service -->
                </div><!-- /.col-md-5 -->











                <div class="col-md-7">
                    <div class="wellcome-content-left">
                        <div class="wellcome-filter-area">
                            <ul class="nav nav-tabs">
                               
                            </ul>
                        </div><!-- /.wellcome-filter -->

                         <!-- Tab panes -->
                        <div class="tab-content wellcome-tab">
                           <div role="tabpanel" class="tab-pane fade in active" id="car-related">
                               <div class="row">
                                    <div class="col-md-6">
                                       <div class="service-block wc">
                                            <a href="#">
                                                <i class="renticon renticon-car-small"></i>
                                            </a>
                                            <h2 class="service-title">Fasilitas Terbaik</h2>
                                            <p class="discription">Kami memiliki fasilitas terbaik di setiap armada kami seperti charger, karoke di bus, fasilitas tambahan sesuai yang diminta dan fasilitas lainnya.</p>
                                       </div><!-- /.service-block -->
                                   </div><!-- /.col-md-6 -->

                                   <div class="col-md-6">
                                       <div class="service-block wc">
                                            <a href="#">
                                                <i class="renticon renticon-star"></i>
                                            </a>
                                            <h2 class="service-title">Kualitas Armada Terbaik</h2>
                                            <p class="discription">Kami memiliki Armada Terbaik yang siap menemani wisata anda</p>
                                       </div><!-- /.service-block -->
                                   </div><!-- /.col-md-6 -->

                                   <div class="clearfix"></div><!-- /.clearfix -->

                                   <div class="col-md-6">
                                       <div class="service-block wc">
                                            <a href="#">
                                                <i class="renticon renticon-support"></i>
                                            </a>
                                            <h2 class="service-title">Pelayanan Yang Ramah dan terbaik</h2>
                                            <p class="discription">Kami selalu memberikan pelayanan yang terbaik untuk konsumen dan pengguna armada kami.</p>
                                       </div><!-- /.service-block -->
                                   </div><!-- /.col-md-6 -->

                                   <div class="col-md-6">
                                       <div class="service-block wc">
                                            <a href="#">
                                                <i class="renticon renticon-carefull-activity"></i>
                                            </a>
                                            <h2 class="service-title">Manajemen Berkualitas</h2>
                                            <p class="discription">Kami Memiliki manajemen yang berkualitas yang senantiasa memberikan yang terbaik untuk anda.</p>
                                       </div><!-- /.service-block -->
                                   </div><!-- /.col-md-6 -->
                               </div><!-- /.row -->
                            </div> <!-- /.home -->

                            
                        </div><!-- /.Tab panes -->
                    </div><!-- /.welcome-content-left -->
                </div><!-- /.col-md-7 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
    <!-- /.check-vehicle-block-->

    <div class="vehicle-multi-border yellow-black"></div><!-- /.vehicle-multi-border -->
        
    

    <div class="about-main-content mr-top-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-top-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="heading-content-three">
                                    
                                </div><!-- /.section-title-area -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                        <div class="row">
                            <div class="col-md-12">
                               
                            </div><!-- /.col-md-12 -->
                            <div class="col-md-6">
                            <?php foreach ($sambutan->result() as $row) :?>
                                <div class="about-content-left">
                                <img src="<?php echo base_url().'assets/images/'.$row->sambutan_photo;?>" style="height:500px;" alt="car-item" />
                                
                                <p></p>  
                                <p>Direktur Utama PT. Banter Sarana Prima <br>  <?php echo $row->pemberi_sambutan;?> </p>  
                                
                                </div>
                                <?php endforeach;?>
                                <!-- /.about-content-left-->
                            </div><!-- /.col-md-5 -->
                            <?php foreach ($sambutan->result() as $row) :?>
                            <div class="col-md-6">
                            <h3>Sambutan Direktur Utama <br> PT. Banter Sarana Prima</h3>
                            
                            <p> <?php echo $row->sambutan_isi;?></p>    
                        </div>
                            <?php endforeach;?>
                            <!-- /.col-md-7 -->
                        </div><!-- /.row -->
                    </div><!-- /.top-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div>
    <!-- ====== Popular Vehicle Block ====== --> 
    <!-- /.Popular Vehicle Block --> 

    <!-- ======Regular-vehicle-block======= --> 
   <!-- /.Regular-Vehicle Block -->

    <!-- ======fun facts block======= -->
    <!-- /.fun-facts-block -->

    <!-- ======driver block======= --> 
    <!-- /.driver-area -->

    <!-- ====== App block ====== --> 
    <!-- /.app-block -->

    <!-- ====== Company Brand Block ====== --> 
   <!-- /.company-logo-block -->
 
   <div class="vehicle-section-divider night-rider">
        <div class="contoiner-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-divider-content">
                        <div class="vehicle-border">
                            <img src="<?php echo base_url()?>tampilan/assets/images/bisbanter.png" alt="car-item" />
                        </div><!-- /.car-border -->
                    </div><!-- /.section-divider-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    
    <!-- ======blog area======= --> 
    <div class="blog-content-block pd-90 bg-gray-color">
        <div class="container">
            <div class="row tb default-margin-bottom theme-red">
                <!-- block-title-area -->
                <div class="col-md-10 block-title-area tb-cell">
                    <div class="heading-content style-one border">
                        <h3 class="subtitle">Berita Terbaru</h3>
                        <h2 class="title">Silahkan - <span>Membaca Artikel Kami</span></h2>
                    </div><!-- /.heading-content-one -->
                </div><!-- /.col-md-10 -->

                <!-- block-navigation-area -->
                <div class="col-md-2 hidden-xs block-navigation-area tb-cell">
                    <div class="item-navigation nav-right">
                        <a href="#" class="previous-item">
                            <i class="fa fa-angle-left"></i> 
                        </a>

                        <a href="#" class="next-item">
                            <i class="fa fa-angle-right"></i> 
                        </a>
                    </div><!-- /.item-navigation -->
                </div><!-- /.col-md-2 -->
            </div><!-- /.row --> 

            <!-- /.vehicle-slider -->
            <div class="vehicle-blog-slider slider-style-two owl-carousel" data-item="[3,2,1,1]">
            <?php foreach ($berita->result() as $row) :?>
                <div class="item">
                    <article class="post">
                        <figure class="post-thumb">
                            <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>">
                                <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" style="height:200px" alt="blog" />
                            </a>
                        </figure><!-- /.post-thumb -->
                        <div class="post-content">  
                            <div class="entry-meta">
                               
                            </div><!-- /.entry-meta -->
                            <h2 class="entry-title">
                                <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo $row->tulisan_judul;?></a>
                            </h2><!-- /.entry-title -->
                            <div class="entry-footer">
                                <div class="entry-footer-meta">
                                    
                                </div><!-- /.entry-footer-meta -->
                            </div><!-- /.entry-footer -->
                        </div><!-- /.post-content -->
                    </article><!-- /.post -->
                </div><!-- /.item -->
                <?php endforeach;?>
                <!-- /.item -->
                
               <!-- /.item -->

                <!-- /.item -->

               <!-- /.item -->
                
            </div><!-- /.vehicle-slider -->
            <!-- block-navigation-area -->
            <div class="block-navigation-area visible-xs-block">
                <div class="view-all-item clearfix">
                    <a href="<?php echo site_url('blog');?>" class="view-all-btn">Lihat Semua Berita</a>
                </div><!-- /.view-all-item -->
            </div><!-- /.block-navigation-area -->
        </div><!-- /.container -->
    </div><!-- /.Blog-area-->


    <div class="blog-content-block pd-90 bg-gray-color">
        <div class="container">
            <div class="row tb default-margin-bottom theme-red">
                <!-- block-title-area -->
                <div class="col-md-10 block-title-area tb-cell">
                    <div class="heading-content style-one border">
                        <h3 class="subtitle">Pengumuman</h3>
                        <h2 class="title">Berikut - <span>Pengumuman Kami</span></h2>
                    </div><!-- /.heading-content-one -->
                </div><!-- /.col-md-10 -->

                <!-- block-navigation-area -->
                <div class="col-md-2 hidden-xs block-navigation-area tb-cell">
                    <div class="item-navigation nav-right">
                        <a href="#" class="previous-item">
                            <i class="fa fa-angle-left"></i> 
                        </a>

                        <a href="#" class="next-item">
                            <i class="fa fa-angle-right"></i> 
                        </a>
                    </div><!-- /.item-navigation -->
                </div><!-- /.col-md-2 -->
            </div><!-- /.row --> 

            <!-- /.vehicle-slider -->
            <div class="vehicle-blog-slider slider-style-two owl-carousel" data-item="[3,2,1,1]">
            <?php foreach ($pengumuman->result() as $row) :?>
                <div class="item">
                    <article class="post">
                        <figure class="post-thumb">
                            <a href="#">
                                <img src="<?php echo base_url().'theme/images/pengumuman.png'?>" style="width:100px;height:100px" alt="blog" />
                            </a>
                        </figure><!-- /.post-thumb -->
                        <div class="post-content">  
                            <div class="entry-meta">
                               
                            </div><!-- /.entry-meta -->
                            <h2 class="entry-title">
                                <a href="#"><?php echo $row->pengumuman_judul;?></a>
                            </h2><!-- /.entry-title -->
                            <div class="entry-footer">
                                <div class="entry-footer-meta">
                                <span class="entry-like"><a href="#"><i class="fa fa-calender"></i><?php echo date("d", strtotime($row->tanggal));?>  <?php echo date("M Y", strtotime($row->tanggal));?>
</a></span>
                                                        <span class="entry-comments"><a href="#"><i class="fa fa-comments"></i><?php echo date("H:i", strtotime($row->tanggal)).' WIB';?>
</a></span>
                                </div><!-- /.entry-footer-meta -->
                                <?php echo $row->pengumuman_deskripsi;?>
                            </div><!-- /.entry-footer -->
                        </div><!-- /.post-content -->
                    </article><!-- /.post -->
                </div><!-- /.item -->
                <?php endforeach;?>
                <!-- /.item -->
                
               <!-- /.item -->

                <!-- /.item -->

               <!-- /.item -->
                
            </div><!-- /.vehicle-slider -->
            <!-- block-navigation-area -->
            <div class="block-navigation-area visible-xs-block">
                <div class="view-all-item clearfix">
                    <a href="<?php echo site_url('blog');?>" class="view-all-btn">Lihat Semua Berita</a>
                </div><!-- /.view-all-item -->
            </div><!-- /.block-navigation-area -->
        </div><!-- /.container -->
    </div>

    <!-- ======footer area======= -->
    <div class="container footer-top-border">
        <div class="vehicle-multi-border yellow-black"></div><!-- /.vehicle-multi-border -->
    </div><!-- /.container -->

    <!-- /.footer-block -->
       <?php echo $footer?>
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->

    <!-- RS5.4 Core JS Files -->
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/jquery.themepunch.revolution.min.js"></script>
  
    <script>
        jQuery(document).ready(function() {
            var $sliderSelector = jQuery(".carrent-slider");
            $sliderSelector.revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "on",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "gyges",
                        enable: true,
                        hide_onmobile: false,
                        hide_onleave: true,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 10,
                            v_offset: 0
                        }
                    }
                },
                responsiveLevels:[1400,1368,992,480],
                visibilityLevels:[1400,1368,992,480],
                gridwidth:[1400,1368,992,480],
                gridheight:[600,600,500,380],
                disableProgressBar:"on"
            });
        });
    </script>

    
	<script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
</body>


</html>
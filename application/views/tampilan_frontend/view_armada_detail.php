<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/car-single-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:42 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    <link rel="apple-touch-icon" href="<?php echo base_url()?>tampilan/assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">   

    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/jquery-ui/jquery-ui.min.css">   
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
    <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
   <!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
   <?php echo $header?>
   <!-- /.header-bottom-area -->

    <!-- ====== Page Header ====== -->
    <div class="page-header background-overlay" style="background-image:url(assets/images/page-header-one.png)">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title">Armada : <?php echo $title;?></h2>
                    <p class="page-description yellow-color">PT. Banter Sarana Prima</p>        
                </div><!-- /.col-md-12 -->
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div>

    <!-- ====== Vehicle Single Block ====== --> 
    <div class="vehicle-single-block vehicle-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="corousel-gallery-content">
                        <div class="gallery">
                             <div class="full-view owl-carousel">
                                  <a class="item" href="#">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarkedua?>" alt="post">
                                  </a>
                                  <a class="item" href="#">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarketiga?>" alt="post">
                                  </a>
                                  <a class="item" href="#">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarkeempat?>" alt="post">
                                  </a>
                                  
                              </div>
                              <h2 class="page-title">Gambar Kondisi Armada<h2>
                            <div class="list-view owl-carousel">
                                  <div class="item">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarkedua?>" alt="post-image">
                                  </div>
                                  <div class="item">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarketiga?>" alt="post-image">
                                  </div>
                                  <div class="item">
                                      <img src="<?php echo base_url().'assets/images/'.$gambarkeempat?>" alt="post-image">
                                  </div>
                                 
                              </div>  
                        </div> <!-- /.gallery-two -->
                    </div> <!-- /.corousel-gallery-content -->

                    <div class="vehicle-single-content">
                        <div class="tb mb-block">
                            <div class="tb-cell mb-block">
                               <h2 class="vehicle-single-title"><?php echo $title;?></h2>
                            </div><!-- /.tb-cell -->
                            <div class="tb-cell mb-block">
                               
                            </div><!-- /.tb-cell -->
                        </div><!-- /.tb -->
                        <div class="clearfix"></div><!-- /.clearfix -->
                        
                        <div class="price-details">
                            <h3 class="details-title">Detail Armada </h3>
                            <ul>
                                <li>Jenis Armada        : <?php echo $jenis;?></li>
                                <li>Kapasitas Armada    : <?php echo $kapasitas;?></li>
                                <li>Jumlah Set          : <?php echo $set;?></li>
                                <li>Fasilitas Armada : <?php echo $fasilitas;?></li>
                              
                               
                            </ul>
                        </div><!-- /.price -->

                        <div class="vehicle-overview">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="overview-title">Deskripsi Armada <?php echo $title;?></h3>
                                    <div class="overview">
                                    <p><?php echo $blog;?></p>
                                    </div><!-- /.vehicle-overview -->
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.overview -->

                        <!-- /.indoor -->
                    </div><!-- /.family-apartment-content -->

                    <div class="hidden-md hidden-lg text-center extend-btn">
                        <span class="extend-icon">
                            <i class="fa fa-angle-down"></i>
                        </span>
                    </div>
                </div> <!-- /.col-md-8 -->

                <div class="col-md-4">
                    <div class="vehicle-sidebar pd-zero">                    
                        <form action="#" method="get" class="advance-search-query search-query-two">
                            <h2 class="form-title">Silahkan Pesan Bus Kepada Kami</h2>
                            <div class="form-content available-filter">
                                <div class="regular-search">
                                <!-- /.form-group -->

                                </div><!-- /.div regular-search -->

                                <!-- /.advance-search -->

                                <div class="check-vehicle-footer">
                                    <a  href="https://api.whatsapp.com/send?phone=6285318274243&text=Hallo%20Selamat%20Datang%20Di%20Bus%20Banter%20Saya%20Ingin%20Memesan%20Bus%20Banter" class="button yellow-button" target="_blank">Pesan Bus</a>                          
                                    
                                </div><!-- /.check-vehicle-footer -->
                            </div><!-- /.from-cotent -->
                        </form><!-- /.advance_search_query -->
                        
                       <!-- /.ads_area -->
                    </div><!-- /.vehicle-sidebar -->
                </div><!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container  -->
    </div><!-- /.Popular Vehicle Block --> 

    <!-- ====== Ralated  Block ====== --> 
    
    <!-- /.ralated-block -->

    <!-- ======footer area======= -->
    <div class="vehicle-section-divider night-rider">
        <div class="contoiner-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-divider-content">
                        <div class="vehicle-border">
                            <img src="<?php echo base_url()?>tampilan/assets/images/bisbanter.png" alt="car-item" />
                        </div><!-- /.car-border -->
                    </div><!-- /.section-divider-content -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.container -->

    <?php echo $footer?>
    <!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->

    <script src="<?php echo base_url()?>tampilan/assets/jquery-ui/jquery-ui.min.js"></script> <!-- main-js -->
</body>

<!-- Mirrored from htmlguru.net/carrent-html/car-single-page.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:45 GMT -->
</html>
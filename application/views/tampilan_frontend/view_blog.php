<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:48 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    
    <link rel="apple-touch-icon" href="<?php echo base_url()?>tampilan/assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">   
</head>

<body>
    <!-- ====== Header Top Area ====== --> 
   <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <div class="header-modal-area">
        <!-- Modal Search -->
        

        <!-- /.overlay-sidebar -->
    </div><!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
    <?php echo $header ?><!-- /.header-bottom-area -->

    <!-- ====== Page Header ====== -->
    <div class="page-header nevy-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">                
                    <h2 class="page-title">Artikel</h2>
                    <p class="page-description">Silahkan Menikmati Artikel Kami</p>        
                </div><!-- /.col-md-12 -->
            </div><!-- /.row-->
        </div><!-- /.container-fluid -->           
    </div>

    <!-- ======blog-area====== --> 
   <div class="blog-block bg-gray-color">
       <div class="container">
           <div class="row">
                <div class="col-md-8">
                    <div class="post-filter-block clearfix">
                        <div class="post-filter-area clearfix">
                           
                        </div> <!-- /.tab-list -->
                    </div><!-- /.post-filter-block -->

                   <div class="blog-content-left">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="row">
                                <?php foreach ($data->result() as $row) : ?>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <article class="post">
                                            <figure class="post-thumb">
                                                <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>">
                                                    <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" style="height:200px" alt="blog" />
                                                </a>
                                            </figure><!-- /.post-thumb -->
                                            <div class="post-content">  
                                                <!-- /.entry-meta -->
                                                <h2 class="entry-title">
                                                    <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo $row->tulisan_judul;?></a>
                                                </h2><!-- /.entry-title -->
                                                <div class="entry-footer">
                                                    <div class="entry-footer-meta">
                                                       
                                                    </div><!-- /.entry-footer-meta -->
                                                </div><!-- /.entry-footer -->
                                            </div><!-- /.post-content -->
                                        </article><!-- /.post -->
                                    </div><!-- /.col-md-6 -->
                                    <?php endforeach;?>
             
                                    

                                    <div class="clearfix"></div><!-- /.clearfix -->

                                    <!-- /.col-md-6 --> 

                                   <!-- /.col-md-6 -->
                                    
                                </div><!-- /.row -->

                                <!-- /.row -->
                            </div> <!-- /.home -->
                              
                            <!-- /.profile -->
                            
                            <!-- /.messages -->
                        </div> <!-- /.tab-content -->
                   </div><!-- /.blog-content-left -->
                </div><!-- /.col-md-8 -->

                <div class="col-md-4 blog-sidebar">
                    <div class="blog-content-right nevy-bg">
                        <div class="widget widget_popular_posts clearfix">
                            
                            <div class="widget-content">
                            
             



                              <!-- /.post-content -->
                                <!-- /.post-content -->
                                <!-- /.post-content -->
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget widget_popular_post -->

                        <div class="widget widget_categories clearfix">
                            <h4 class="widget-title">Daftar Kategori Artikel</h4> <!-- /.widget-title -->
                            <ul>
                            <?php foreach ($category->result() as $row) : ?>
                                <li>
                                    <a href="#"><?php echo $row->kategori_nama;?></a>
                                   
                                </li>
                                <?php endforeach;?>
             
                            </ul>
                        </div> <!-- /.widget_categories -->

                       <!-- /.widget_ads -->

                         <!-- /.widget_tagcloud -->
                    </div><!-- /.blog-content-right -->
               </div><!-- /.col-md-4 -->
           </div><!-- /.row -->
       </div><!-- /.container -->
   </div><!-- /.blog-main-content -->
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="pagination-center">
                                <?php echo $page;?>
                                </ul>
                            </div>
    <!-- ======footer area======= -->
    <div class="container footer-top-border">
        <div class="vehicle-multi-border yellow-black"></div><!-- /.vehicle-multi-border -->
    </div><!-- /.container -->

    <?php echo $footer?>
    <!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->
</body>

<!-- Mirrored from htmlguru.net/carrent-html/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:49 GMT -->
</html>
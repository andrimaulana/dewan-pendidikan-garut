<header class="header-nav-area">
        <div class="container">        
            <div class="row">
                <div class="col-md-3 col-sm-10 col-xs-10">
                    <div class="site-logo">
                        <a href="#"><img src="<?php echo base_url()?>tampilan/assets/images/logobanter.png" alt="logo" /></a>
					
					</div><!-- /.logo -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-9 col-sm-2 col-xs-2 pd-right-0">
                    <nav class="site-navigation top-navigation nav-style-one">
                        <div class="menu-wrapper">
                            <div class="menu-content">
                                <ul class="menu-list">
                                    <li>
                                        <a href="<?php echo site_url('');?>">Home</a>
                                        
                                    </li>
                                    <li>
                                        <a href="<?php echo site_url('tentang');?>">Tentang Kami</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo site_url('anggota');?>">Driver dan Staf</a>
                                    </li>

                                     <li>
                                        <a href="https://api.whatsapp.com/send?phone=6285320903771&text=Hallo%20Selamat%20Datang%20Di%20Bus%20Banter%20Saya%20Ingin%20Memesan%20Bus%20Banter" target="_blank">Pesan Bus</a>
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo site_url('blog');?>">Artikel</a>
                                    </li>

                                    <li>
                                        <a href="<?php echo site_url('armada');?>">Armada</a>
                                    </li>
                                    
                                </ul> <!-- /.menu-list -->
                            </div> <!-- /.menu-content-->
                        </div> <!-- /.menu-wrapper --> 
                    </nav><!-- /.site-navigation -->
                    <!--Mobile Main Menu-->
                    <div class="mobile-menu-main hidden-md hidden-lg">
                        <div class="menucontent overlaybg"> </div>
                        <div class="menuexpandermain slideRight">
                            <a id="navtoggole-main" class="animated-arrow slideLeft menuclose">
                                <span></span>
                            </a>
                        </div><!--/.menuexpandermain-->

                        <div id="mobile-main-nav" class="mb-navigation slideLeft">
                            <div class="menu-wrapper">
                                <div id="main-mobile-container" class="menu-content clearfix"></div>
                            </div>
                        </div><!--/#mobile-main-nav-->
                    </div><!--/.mobile-menu-main-->
                </div><!-- /.col-md-9 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </header>
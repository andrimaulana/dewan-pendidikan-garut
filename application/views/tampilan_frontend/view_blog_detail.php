<!doctype html>
<html lang="en">


<!-- Mirrored from htmlguru.net/carrent-html/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:49 GMT -->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Specific Meta
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Texicab is a modern presentation HTML5 Car Rent template.">
    <meta name="keywords" content="HTML5, Template, Design, Development, Car Rent" />
    <meta name="author" content="">

    <!-- Titles
    ================================================== -->
    <title>Bus Banter Garut</title>
  

<?php
        function limit_words($string, $word_limit){
            $words = explode(" ",$string);
            return implode(" ",array_splice($words,0,$word_limit));
        }
    ?>
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="<?php echo base_url()?>tampilan/assets/images/favicon.png">
    
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.html">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">

    <!-- Custom Font
    ================================================== -->
    <link href="https://fonts.googleapis.com/css?family=Exo:400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cRoboto+Slab:400,700" rel="stylesheet">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/plugins.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url()?>tampilan/assets/css/color-schemer.css">   
</head>

<body class="single">
    <!-- ====== Header Top Area ====== --> 
    <!-- /.head-area -->

    <!-- ======= Header Modal Area =======-->
    <div class="header-modal-area">
        <!-- Modal Search -->
        

      <!-- /.overlay-sidebar -->
    </div><!-- /.header-modal-area -->

    <!-- ====== Header Nav Area ====== --> 
    <?php echo $header?>
    <!-- /.header-bottom-area -->

    <!-- ====== Page Header ====== -->
    <div class="page-header background nevy-bg">
      <!-- /.container-fluid -->         

      <div class="col-md-12">                
                    <h2 class="page-title"><a href="<?php echo site_url('artikel/'.$slug);?>"><?php echo $title;?></a></h2>
                    <p class="page-description"></p>        
                </div>
    </div>

    <!-- ======Block Single Block====== --> 
    <div class="blog-single-block bg-gray-color pd-btm-60">
        <div class="container">
            <div class="row">
                <!-- Blog single Content -->
                <div class="col-md-8">
                    <div class="single-main-content">
                        <article class="post-content">
                            <div class="entry-header">
                               
                            </div><!-- /.entry-header -->

                            <div class="author-content clearfix hidden-xs">
                                <div class="entry-author pull-left">
                                    
                                    <div class="author-details">
                                        <h4 class="author-name">Penulis</h4>
                                        <p class="author-designation"><?php echo $author;?></p>
                                    </div>
                                </div><!-- /.pull-left -->
                                
                                <div class="post-navigation block-navigation-area yellow-theme">
                                   <!-- /.item-navigation -->
                                </div><!-- /.post-navigation -->
                            </div><!-- /.author-content -->
    
                            <figure class="post-thumb">
                                <img src="<?php echo base_url().'assets/images/'.$image?>" alt="blog">
                            </figure><!-- /.post-thumb -->

                            <div class="single-post">
                                <div class="entry-meta">
                                    <div class="entry-date">
                                        <div class="meta-title">Tanggal</div> 
                                        <a href="#"><?php echo $tanggal;?></a>
                                    </div>
                            
                                    <div class="entry-category">
                                        <div class="meta-title">Kategori</div> 
                                        <a href="#"><?php echo $kategori;?></a>,
                                       
                                    </div>      

                                              
                                </div><!-- /.entry-meta -->

                                <div class="entry-content">
                                <p><?php echo $blog;?></p>

                                    <div class="gallery gallery-columns-4">
                                      <!-- /.gallery-item -->
                                    </div><!-- /.gallery -->
                                
                                    <p>Nommodo ligula eget dolor Morlem ipsum dolor sit amet nec, conse ctetuer adipi-scing elit. Aenean commod ligula eget dolor Cum sociis natoque penatibus et magnis dis parturient montes. Morlem ipsum dolor sit amet nec penatib et magnis dis parturient montes. Morlem ipstium dolor sit amet nec, conse ctetuer adipiscing elit. Aenean commodo ligulaits eget is dolor. Aenean massa. Cum sociis nato que pena tibus et magnis dis partu rient montes. Morlem ipsum dolor set amet nec, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</p>
                                </div><!-- /.entry-content -->
                            </div><!-- /.single-post -->

                            <!-- /.entry-share -->
                        </article><!-- /.post -->
                    </div><!-- /.single-main -->

                    <!-- ======Related Post BLock======= --> 

                    <div id="comments" class="comments-area color-white">
                        <div class="comments-main-content">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="comments-title">Komentar</h3><!--/.comments-title-->
                                </div><!--/.col-md-12-->
                            </div><!--/.row-->
                            <div class="row">
                                <div class="col-md-12">
                                    <ol class="comment-list">
                                    <?php
                                  
                                  foreach ($show_komentar->result() as $row) :
                                  
                                ?>
                                        <li class="comment">
                                            <div class="comment-body">
                                                <div class="comment-meta">
                                                    <div class="comment-author vcard">
                                                    
                                                        <div class="author-img">
                                                            <img alt="" src="<?php echo base_url()?>tampilan/assets/images/comment/comment-one.png" class="avatar photo">
                                                        </div>
                                                    </div><!--/.comment-author-->
                                                    <div class="comment-metadata"><b class="author"><?php echo $row->komentar_nama;?></b>
                                                        <span class="date"><?php echo date("d M Y H:i", strtotime($row->komentar_tanggal));?></span>
                                                    </div><!--/.comment-metadata-->
                                                </div><!--/.comment-meta-->
                                                <div class="comment-details">
                                                    <div class="comment-content">
                                                        <p><?php echo $row->komentar_isi;?> </p>
                                                    </div><!--/.comment-content-->
                                                    <!--/.reply-->
                                                </div><!-- /.comment-details-->
                                            </div><!--/.comment-body-->
                                            
                                            
                                            
                                            <?php
                                      $komentar_id=$row->komentar_id;
                                      $query=$this->db->query("SELECT * FROM komentar WHERE komentar_status='1' AND komentar_parent='$komentar_id' ORDER BY komentar_id ASC");
                                      foreach ($query->result() as $res) :
                                        
                                  ?> 

                            
                                            <ol class="children">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <div class="comment-meta">
                                                            <div class="comment-author vcard">
                                                                <div class="author-img">
                                                                    <img alt="" src="<?php echo base_url()?>tampilan/assets/images/comment/comment-two.png" class="avatar photo">
                                                                </div>
                                                            </div><!--/.comment-author-->
                                                            <div class="comment-metadata"><b class="author"><?php echo $res->komentar_nama;?></b>
                                                                <span class="date"><?php echo date("d M Y H:i", strtotime($res->komentar_tanggal));?></span>
                                                            </div><!--/.comment-metadata-->
                                                        </div><!--/.comment-meta-->
                                                        <div class="comment-details">
                                                            <div class="comment-content">
                                                                <p><?php echo $res->komentar_isi;?> </p>
                                                            </div><!--/.comment-content-->
                                                            <!--/.reply-->
                                                        </div><!-- /.comment-details-->
                                                    </div><!--/.comment-body-->
                                                </li><!--/.comment-->
                                            </ol><!--/.children-->
                                        </li><!--/.comment-body-->
                                        
                                        
                                        <?php endforeach;?>
                                        <?php endforeach;?>
                                        
                                        
                                        <!--/.comment-body-->
                                    </ol><!--/.comment-list-->
                                </div><!--/.col-md-12-->
                            </div><!--/.row-->
                        </div><!-- /.comments-main-content -->
                    </div>

                     <div id="respond" class="comment-respond box-radius bg-gray-color">
                        <div class="comments-main-content bg-white-color">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="comment-reply-title">Silahkan Berikan Komentar</h3><!--/.comment-reply-title-->
                                </div><!--/.col-md-12-->
                            </div><!--/.row-->
                            <div class="row">
                                <div class="col-md-12">
                                <?php echo $this->session->flashdata('msg');?>
                                    <form action="<?php echo site_url('blog/komentar');?>" method="post" id="comment_form" name="commentForm">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 padding-right">
                                                <p>
                                                    <input type="text" name="nama" aria-required="true" placeholder="Masukan Nama" class="form-controllar" required> </p>
                                            </div><!--/.col-md-6-->
                                            <div class="col-md-6 col-sm-6">
                                                <p>
                                                    <input type="text" name="email" aria-required="true" placeholder="Masukan Email" class="form-controllar"> </p>
                                            </div><!--/.col-md-6-->
                                            <!--/.col-md-12-->
                                            <div class="col-md-12">
                                                <p>
                                                    <textarea name="komentar" id="message" aria-required="true" rows="1" cols="10" placeholder="Masukan Komentar" class="form-controllar"></textarea>
                                                </p>
                                            </div><!--/.col-md-12-->
                                            <div class="col-md-12">
                                                <p class="form-submit ">
                                                <input type="hidden" name="id" value="<?php echo $id;?>" required>
                                                    <button type="submit" id="submit" class="button nevy-bg">Berikan Komentar</button>
                                                </p>
                                            </div><!--/.col-md-12-->
                                        </div><!--/.row-->
                                    </form><!--/#comment_form-->
                                </div><!--/.col-md-12-->
                            </div><!--/.row-->
                        </div><!-- /.comments-main-content -->
                    </div>
                    
                    <div class="blog-content-block pd-90 bg-gray-color">
        <div class="container">
            <div class="row tb default-margin-bottom theme-red">
                <!-- block-title-area -->
                <div class="col-md-10 block-title-area tb-cell">
                    <div class="heading-content style-one border">
                        <h3 class="subtitle">Artikel Kami Lainnya</h3>
                        <h2 class="title">Silahkan - <span>Membaca Artikel Kami</span></h2>
                    </div><!-- /.heading-content-one -->
                </div><!-- /.col-md-10 -->

                <!-- block-navigation-area -->
                <div class="col-md-2 hidden-xs block-navigation-area tb-cell">
                    <div class="item-navigation nav-right">
                        <a href="#" class="previous-item">
                            <i class="fa fa-angle-left"></i> 
                        </a>

                        <a href="#" class="next-item">
                            <i class="fa fa-angle-right"></i> 
                        </a>
                    </div><!-- /.item-navigation -->
                </div><!-- /.col-md-2 -->
            </div><!-- /.row --> 

            <!-- /.vehicle-slider -->
            <div class="vehicle-blog-slider slider-style-two owl-carousel" data-item="[3,2,1,1]">
            <?php foreach ($berita->result() as $row) :?>
                <div class="item">
                    <article class="post">
                        <figure class="post-thumb">
                            <a href="#">
                                <img src="<?php echo base_url().'assets/images/'.$row->tulisan_gambar;?>" style="height:300px" alt="blog" />
                            </a>
                        </figure><!-- /.post-thumb -->
                        <div class="post-content">  
                            <div class="entry-meta">
                               
                            </div><!-- /.entry-meta -->
                            <h2 class="entry-title">
                                <a href="<?php echo site_url('artikel/'.$row->tulisan_slug);?>"><?php echo $row->tulisan_judul;?></a>
                            </h2><!-- /.entry-title -->
                            <div class="entry-footer">
                                <div class="entry-footer-meta">
                                    
                                </div><!-- /.entry-footer-meta -->
                            </div><!-- /.entry-footer -->
                        </div><!-- /.post-content -->
                    </article><!-- /.post -->
                </div><!-- /.item -->
                <?php endforeach;?>
                <!-- /.item -->
                
               <!-- /.item -->

                <!-- /.item -->

               <!-- /.item -->
                
            </div><!-- /.vehicle-slider -->
            <!-- block-navigation-area -->
            <div class="block-navigation-area visible-xs-block">
                <!-- /.view-all-item -->
            </div><!-- /.block-navigation-area -->
        </div><!-- /.container -->
    </div><!-- /.related-blog-->

                    <!-- /.comments-area -->

                    <!-- /.comment-respond -->
                </div><!-- /.col-md-8 -->
                
                <!-- Blog Sidebar Content -->
                
                <div class="col-md-4 blog-sidebar">
                    <div class="blog-content-right nevy-bg">
                        <div class="widget widget_popular_posts clearfix">
                            
                            <div class="widget-content">
                            
             



                              <!-- /.post-content -->
                                <!-- /.post-content -->
                                <!-- /.post-content -->
                            </div><!-- /.widget-content -->
                        </div><!-- /.widget widget_popular_post -->

                        <div class="widget widget_categories clearfix">
                            <h4 class="widget-title">Daftar Kategori Artikel</h4> <!-- /.widget-title -->
                            <ul>
                            <?php foreach ($category->result() as $row) : ?>
                                <li>
                                    <a href="#"><?php echo $row->kategori_nama;?></a>
                                   
                                </li>
                                <?php endforeach;?>
             
                            </ul>
                        </div> <!-- /.widget_categories -->

                       <!-- /.widget_ads -->

                         <!-- /.widget_tagcloud -->
                    </div><!-- /.blog-content-right -->
               </div>
               <!-- /.col-md-4 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.blog-main-content -->
    
    <!-- ======footer area======= -->
    <div class="container footer-top-border">
        <div class="vehicle-multi-border yellow-black"></div><!-- /.vehicle-multi-border -->
    </div><!-- /.container -->

   <?php echo $footer?><!-- /.footer-block -->
       
    <!-- All The JS Files
    ================================================== --> 
    <script src="<?php echo base_url()?>tampilan/assets/js/plugins.min.js"></script>
    <script src="<?php echo base_url()?>tampilan/assets/js/carrent.min.js"></script> <!-- main-js -->
</body>

<!-- Mirrored from htmlguru.net/carrent-html/blog-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Oct 2020 10:46:51 GMT -->
</html>